﻿
function StandardDeviationGraph() {

    this.graph;
    this.tip;
    this.xPadding = 40;
    this.yPadding = 10;
    this.data = [];
    this.dots = [];
    this.OneSDAreaColor = 'gray';
    this.SecondSDAreaColor = 'yellow';
    this.ThirdSDAreaColor = 'red';
    this.graphLineColor = "#f00";
    this.dotsColor = '#333';
    this.waterMarkMsg = "Water Mark";
    this.isToolTipEnable = true;

}

// Return the y pixel for a graph point
StandardDeviationGraph.prototype.getYPixel = function (val, max, min) {

    val = val - min;
    max = max - min;
    var perHeight = (val / max) * 100;
    var height = (perHeight * this.graph.height()) / 100;

    return (this.graph.height() - height - this.yPadding);
}

// Return the x pixel for a graph point
StandardDeviationGraph.prototype.getXPixel = function (val) {
    return ((this.graph.width() - (this.xPadding + 40)) / this.data.values.length) * val + (this.xPadding * 1.5);
}

// show tooltip when mouse hovers over dot
StandardDeviationGraph.prototype.handleMouseMove = function (e) {

    var tipCanvas = this.tip;
    var tipCtx = tipCanvas[0].getContext("2d");

    var canvasOffset = this.graph.offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);

    // Put your mousemove stuff here
    var hit = false;

    for (var i = 0; i < this.dots.length; i++) {

        var dot = this.dots[i];
        var dx = mouseX - dot.x;
        var dy = mouseY - dot.y;


        if (dx * dx + dy * dy < dot.rXr) {

            tipCanvas.css("left", (dot.x) + "px");
            tipCanvas.css("top", (dot.y - 0) + "px");
            tipCtx.clearRect(0, 0, tipCanvas.width(), tipCanvas.height());
            tipCtx.fillText(dot.tip, 5, 15);
            hit = true;
        }
    }
    if (!hit) {
        tipCanvas.css("left", "-200px");
    }
}


StandardDeviationGraph.prototype.init = function () {


    var graph = this.graph;
    var c = graph[0].getContext('2d');

    c.globalAlpha = 0.4;
    c.font = 'italic 30px Arial';
    c.textAlign = 'center';
    c.fillStyle = 'red';
    c.fillText(this.waterMarkMsg, graph.width() / 2, graph.height() / 2 - 30);


    c.lineWidth = 2;
    c.strokeStyle = '#333';
    c.font = 'italic 8pt sans-serif';
    c.textAlign = "center";

    /*Calculate Mean value*/
    var MeanValue = 0;
    var totalElementHasCount = 0;
    for (var i = 0; i < this.data.values.length; i++) {
        if (this.data.values[i].isCount == 1) {
            MeanValue = MeanValue + this.data.values[i].Y;
            totalElementHasCount++;
        }
    }
    MeanValue = MeanValue / totalElementHasCount;

    /*Calculate 1SD value*/
    var OneSDValue = 0;
    for (var i = 0; i < this.data.values.length; i++) {
        if (this.data.values[i].isCount == 1) {
            OneSDValue = OneSDValue + ((MeanValue - this.data.values[i].Y) * (MeanValue - this.data.values[i].Y));
        }
    }
    OneSDValue = OneSDValue / (totalElementHasCount - 1);
    OneSDValue = Math.sqrt(OneSDValue);

    var sdHeight = (graph.height() - this.yPadding) / 6;

    c.fillStyle = this.ThirdSDAreaColor;
    c.beginPath();
    c.rect(this.xPadding, (0 * sdHeight), graph.width() - (this.xPadding + 40), sdHeight);
    c.fill();


    c.fillStyle = this.SecondSDAreaColor;
    c.beginPath();
    c.rect(this.xPadding, (1 * sdHeight), graph.width() - (this.xPadding + 40), sdHeight);
    c.fill();

    c.fillStyle = this.OneSDAreaColor;
    c.beginPath();
    c.rect(this.xPadding, (2 * sdHeight), graph.width() - (this.xPadding + 40), sdHeight * 2);
    c.fill();

    c.fillStyle = this.SecondSDAreaColor;
    c.beginPath();
    c.rect(this.xPadding, (4 * sdHeight), graph.width() - (this.xPadding + 40), sdHeight);
    c.fill();

    c.fillStyle = this.ThirdSDAreaColor;
    c.beginPath();
    c.rect(this.xPadding, (5 * sdHeight), graph.width() - (this.xPadding + 40), sdHeight);
    c.fill();
    c.globalAlpha = 1;

    var midLinePos = graph.height() / 2;
    c.lineWidth = 2;
    c.strokeStyle = '#333';
    c.beginPath();
    c.moveTo(this.xPadding, midLinePos);
    c.lineTo(graph.width() - this.xPadding, midLinePos);

    c.moveTo(this.xPadding, 0);
    c.lineTo(this.xPadding, graph.height() - this.yPadding);

    c.moveTo(graph.width() - this.xPadding, 0);
    c.lineTo(graph.width() - this.xPadding, graph.height() - this.yPadding);

    c.stroke();

    /*Define y axis*/
    c.font = 'italic 8pt sans-serif';
    c.textAlign = "center";
    c.textAlign = "right";
    c.textBaseline = "middle";

    c.fillText((MeanValue + (OneSDValue * 3)).toFixed(2), this.xPadding - 10, (0 * sdHeight) + 4);
    c.fillText((MeanValue + (OneSDValue * 2)).toFixed(2), this.xPadding - 10, (1 * sdHeight) + 4);
    c.fillText((MeanValue + (OneSDValue * 1)).toFixed(2), this.xPadding - 10, (2 * sdHeight) + 4);
    c.fillText((MeanValue + (OneSDValue * 0)).toFixed(2), this.xPadding - 10, (3 * sdHeight) + 4);
    c.fillText((MeanValue + (OneSDValue * -1)).toFixed(2), this.xPadding - 10, (4 * sdHeight) + 4);
    c.fillText((MeanValue + (OneSDValue * -2)).toFixed(2), this.xPadding - 10, (5 * sdHeight) + 4);
    c.fillText((MeanValue + (OneSDValue * -3)).toFixed(2), this.xPadding - 10, (6 * sdHeight) - 4);


    c.fillText('+3 SD', graph.width() - 10, (0 * sdHeight) + 4);
    c.fillText('+2 SD', graph.width() - 10, (1 * sdHeight) + 4);
    c.fillText('+1 SD', graph.width() - 10, (2 * sdHeight) + 4);
    c.fillText('Mean', graph.width() - 10, (3 * sdHeight) + 4);
    c.fillText('-1 SD', graph.width() - 10, (4 * sdHeight) + 4);
    c.fillText('-2 SD', graph.width() - 10, (5 * sdHeight) + 4);
    c.fillText('-3 SD', graph.width() - 10, (6 * sdHeight) - 4);

    // Draw the X value texts
    for (var i = 0; i < this.data.values.length; i++) {
        c.fillText(this.data.values[i].X, this.getXPixel(i), this.graph.height() - this.yPadding + 5);
    }

    // Draw the line graph
    var max = (MeanValue + (OneSDValue * 3));
    var min = (MeanValue + (OneSDValue * -3));


    // define tooltips for each data point
    for (var i = 0; i < this.data.values.length; i++) {
        this.dots.push({
            x: this.getXPixel(i),
            y: this.getYPixel(this.data.values[i].Y, max, min),
            r: 4,
            rXr: 16,
            color: "red",
            tip: this.data.values[i].Y
        });
        this.MeanValue = this.MeanValue + this.data.values[i].Y;

    }

    c.strokeStyle = this.graphLineColor;
    c.beginPath();
    c.moveTo(this.getXPixel(0), this.getYPixel(this.data.values[0].Y, max, min));



    for (var i = 1; i < this.data.values.length; i++) {
        if (this.data.values[i].isCount == 1) {
            c.lineTo(this.getXPixel(i), this.getYPixel(this.data.values[i].Y, max, min));
        }
    }
    c.stroke();


    // Draw the dots
    c.fillStyle = this.dotsColor;

    for (var i = 0; i < this.data.values.length; i++) {
        c.beginPath();
        c.arc(this.getXPixel(i), this.getYPixel(this.data.values[i].Y, max, min), 4, 0, Math.PI * 2, true);
        c.fill();

    }

    if (this.isToolTipEnable) {
        graph.mousemove(function (e) {
            this.handleMouseMove(e);
        }.bind(this));
    }
}